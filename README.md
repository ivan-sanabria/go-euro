# Go Euro - Interview Question 

version 1.7.0 - 21/01/2023

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/go-euro.svg)](http://bitbucket.org/ivan-sanabria/go-euro/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/go-euro.svg)](http://bitbucket.org/ivan-sanabria/go-euro/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_go-euro&metric=alert_status)](https://sonarcloud.io/project/overview?id=Iván-Camilo-Sanabria-Rincón_go-euro)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_go-euro&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=Iván-Camilo-Sanabria-Rincón_go-euro)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_go-euro&metric=coverage)](https://sonarcloud.io/component_measures?id=Iván-Camilo-Sanabria-Rincón_go-euro&metric=coverage)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_go-euro&metric=ncloc)](https://sonarcloud.io/code?id=Iván-Camilo-Sanabria-Rincón_go-euro)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_go-euro&metric=sqale_index)](https://sonarcloud.io/component_measures?metric=Maintainability&id=Iván-Camilo-Sanabria-Rincón_go-euro)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_go-euro&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=Iván-Camilo-Sanabria-Rincón_go-euro)

## Introduction

Test question designed to filter programming candidates over go-euro interview. The assignment of the test is:

*"Create a Java command line tool that takes an input parameter as string, which call the **JSON** api:*
*http://api.goeuro.com/api/v2/position/suggest/en/PARAMETER then parses the response and store the results in **csv** 
file containing the values: _id, name, type, latitude, longitude."*

The test should cover the following requirements:

1. Program should use **Java** and open source libraries.
2. Code should be in **git** repository.
3. Error handling in any case.
4. **Unit test** cases over implementation.

## Requirements

- JDK 14.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **UserController.java**.

## Running Application on Terminal

To run the application on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean compile assembly:single
    java -jar target/go-euro-1.7.0.jar Berlin
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

## Example of Supported Commands

These are the supported commands of the application:

```bash
    # Generate CSV file for Berlin city
    java -jar target/go-euro-1.7.0.jar Berlin
    # Generate CSV files for Berlin and Paris cities
    java -jar target/go-euro-1.7.0.jar Berlin Paris
    # Generate CSV files for Berlin, Paris and Zurich cities
    java -jar target/go-euro-1.7.0.jar Berlin Paris Zurich
```

# Contact Information

Email: icsanabriar@googlemail.com