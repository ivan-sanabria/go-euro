/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.go.euro.api.parser;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * Class to transform JSON input to csv file.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CsvParser {

    /**
     * Define default constructor of CsvParser.
     */
    public CsvParser() {}

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(CsvParser.class);

    /**
     * Define csv file separator.
     */
    private static final String SEPARATOR = ",";

    /**
     * Define extension of csv file.
     */
    private static final String EXTENSION = ".csv";

    /**
     * Generate the csv file using the given cities JSON array.
     *
     * @param result Json array with the data to put into the csv file.
     * @throws IOException Thrown when the writer is not able to create the csv file.
     */
    public void generateCsv(final JsonArray result) throws IOException {

        if (null != result && 0 < result.size()) {

            final StringBuilder builder = new StringBuilder();

            buildCityOutput(result, builder);
            printToCsv(builder);

        } else {

            LOG.warn("The given array to put into the csv file was empty. No file was generated.");
        }
    }

    /**
     * Print the given output into a csv file with uui name.
     *
     * @param output Output is going to be printed into the csv file.
     * @throws IOException Thrown when the writer is not able to create the csv file.
     */
    private void printToCsv(final StringBuilder output) throws IOException {


        final String uuid = UUID.randomUUID()
                .toString();

        final String name = uuid + EXTENSION;

        try (PrintWriter writer = new PrintWriter(name, StandardCharsets.UTF_8)) {

            writer.append(
                    output.toString());
        }

        LOG.info("CSV {} generation file is done.", name);
    }

    /**
     * Build the output is going to be included on the csv file using the given JSON array.
     *
     * @param result  Json array with the data to put into the csv file.
     * @param builder Builder which contains the the csv output.
     */
    private void buildCityOutput(final JsonArray result, final StringBuilder builder) {

        for (int a = 0; a < result.size(); a++) {

            final JsonObject current = result.get(a)
                    .getAsJsonObject();

            builder.append(current.get("_id"))
                    .append(SEPARATOR);

            builder.append(current.get("name"))
                    .append(SEPARATOR);

            builder.append(current.get("type"))
                    .append(SEPARATOR);

            buildLocationOutput(builder, current);

            builder.append(
                    System.lineSeparator());
        }
    }

    /**
     * Build the output of a location which is going to be included on the csv file.
     *
     * @param builder Builder which contains the the csv output.
     * @param current Current element of the Json array used to extract location.
     */
    private void buildLocationOutput(final StringBuilder builder, final JsonObject current) {

        if (current.has("geo_position")) {

            final JsonObject location = current.getAsJsonObject("geo_position");

            builder.append(
                    location.get("latitude"));

            builder.append(SEPARATOR);

            builder.append(
                    location.get("longitude"));

        } else {

            builder.append("null")
                    .append(SEPARATOR);

            builder.append("null");
        }
    }

}
