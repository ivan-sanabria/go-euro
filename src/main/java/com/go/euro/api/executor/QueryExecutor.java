/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.go.euro.api.executor;

import com.go.euro.api.external.ApiAdaptor;
import com.go.euro.api.parser.CsvParser;
import com.google.gson.JsonArray;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Class to handle queries against api using different threads with multiple instances of adaptors.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class QueryExecutor implements Runnable {

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(QueryExecutor.class);

    /**
     * Adaptor instance used to execute the request against point api.
     */
    private ApiAdaptor adaptor;

    /**
     * Parser used to generate csv file.
     */
    private CsvParser parser;

    /**
     * Query to execute against the api.
     */
    private String keyword;

    /**
     * Query constructor using keyword as parameter.
     *
     * @param adaptor Adaptor instance to make request to the api.
     * @param parser  Parser used to generate csv file.
     * @param keyword keyword to execute search against the api.
     */
    public QueryExecutor(final ApiAdaptor adaptor, final CsvParser parser, final String keyword) {
        this.adaptor = adaptor;
        this.parser = parser;
        this.keyword = keyword;
    }

    /**
     * Execute the search using different threads.
     */
    @Override
    public void run() {

        final String encodedKeyword = URLEncoder.encode(
                keyword,
                StandardCharsets.UTF_8);

        final JsonArray result = adaptor.makeRequest(encodedKeyword);

        try {

            parser.generateCsv(result);

        } catch (IOException ioe) {

            LOG.error("CSV generation file failed - ", ioe);
        }
    }

}
