/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.go.euro.api.external;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Class to execute the request to api and return responses on JSON format.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class GoEuroAdaptor implements ApiAdaptor {

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(GoEuroAdaptor.class);

    /**
     * Define api url to make queries.
     */
    private static final String API = "http://api.goeuro.com/api/v2/position/suggest/en/";

    /**
     * Define http client to make request against the api.
     */
    private CloseableHttpClient client;

    /**
     * Adaptor constructor to make multiple queries against the api.
     *
     * @param maxQueries Maximum amount of parallel queries send to the api.
     */
    public GoEuroAdaptor(final int maxQueries) {

        final PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager();

        manager.setMaxTotal(maxQueries);
        manager.setDefaultMaxPerRoute(maxQueries);

        client = HttpClients.custom()
                .setConnectionManager(manager)
                .build();
    }

    /**
     * {@inheritDoc}
     */
    public JsonArray makeRequest(final String keyword) {

        final String query = GoEuroAdaptor.API + keyword;

        final HttpContext context = HttpClientContext.create();
        final HttpGet request = new HttpGet(query);

        try {

            return parseResponse(
                    client.execute(request, context));

        } catch (ClientProtocolException cpe) {

            LOG.error("Error making the request. The protocol is not supported - ", cpe);

        } catch (IOException ioe) {

            LOG.error("Error making request against api - ", ioe);

        } catch (Exception e) {

            LOG.error("Error parsing the response given by the api - ", e);
        }

        return null;
    }

    /**
     * Parse the given response into a JSON array.
     *
     * @param response Response given by the api to parse.
     * @return Json array with the elements given by the response.
     * @throws Exception Thrown when the response is null or invalid.
     */
    private JsonArray parseResponse(final CloseableHttpResponse response) throws Exception {

        JsonArray array = null;

        try (response) {

            final int status = response.getStatusLine()
                    .getStatusCode();

            final HttpEntity entity = response.getEntity();
            final String body = EntityUtils.toString(entity);

            if (200 == status)
                array = JsonParser.parseString(body)
                        .getAsJsonArray();
            else
                LOG.error("Error getting response from api. Status code {} and body {}.", status, body);
        }

        return array;
    }

}
