/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.go.euro.api.external;

import com.google.gson.JsonArray;

/**
 * Interface to specify implementation of adaptors.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public interface ApiAdaptor {

    /**
     * Make the GET request against the api and return the JSON array given in the response.
     *
     * @param keyword keyword to search using given api.
     * @return Json array with the given response. The response could be null.
     */
    JsonArray makeRequest(final String keyword);

}
