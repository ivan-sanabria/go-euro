/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.go.euro.api.controller;

import com.go.euro.api.executor.QueryExecutor;
import com.go.euro.api.external.ApiAdaptor;
import com.go.euro.api.external.GoEuroAdaptor;
import com.go.euro.api.parser.CsvParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class to manage the test case, reading the input from the console and generating the csv after the query is made.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class UserController {

    /**
     * Define default constructor of UserController.
     */
    public UserController() {}

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(UserController.class);

    /**
     * Define maximum number of concurrent queries.
     */
    private static final int MAX_QUERIES = 20;

    /**
     * Define adaptor to request positions.
     */
    private static ApiAdaptor adaptor = new GoEuroAdaptor(MAX_QUERIES);

    /**
     * Define parser to generate csv files.
     */
    private static CsvParser parser = new CsvParser();

    /**
     * Execute the application to generate the csv file using the response from the api.
     *
     * @param args Arguments against the api.
     */
    public static void main(String... args) {

        if (0 == args.length) {

            LOG.error("No query was given as input.");

        } else {

            executeSingleSearch(args);
            executeMultipleSearch(args);
        }
    }

    /**
     * Set the adaptor for testing purposes.
     *
     * @param adaptor Adaptor to make request to Go Euro points api.
     */
    static void setAdaptor(final ApiAdaptor adaptor) {
        UserController.adaptor = adaptor;
    }

    /**
     * Validate the arguments contains more than one value and execute the queries using the multiple thread
     * implementation.
     *
     * @param args Arguments used to make query against api and generate multiple csv file.
     */
    private static void executeMultipleSearch(final String[] args) {

        if (1 < args.length) {

            LOG.warn("More than one query was given at the input. Trying to process them simultaneously.");

            runQueryExecutors(args);
        }
    }

    /**
     * Execute multiple queries at the same time against api to generate multiple csv files using Executors.
     *
     * @param args Arguments used to make query against api and generate multiple csv file.
     */
    private static void runQueryExecutors(final String[] args) {

        final ExecutorService executor = Executors.newFixedThreadPool(MAX_QUERIES);

        for (String keyword : args) {

            final QueryExecutor queryExecutor = new QueryExecutor(adaptor, parser, keyword);
            executor.execute(queryExecutor);
        }

        executor.shutdown();
    }

    /**
     * Validate the arguments contains one value and execute one query against api to generate a csv file.
     *
     * @param args Arguments used to make query against api and generate one csv file.
     */
    private static void executeSingleSearch(final String[] args) {

        if (1 == args.length) {

            final String parameter = args[0];

            final QueryExecutor queryExecutor = new QueryExecutor(adaptor, parser, parameter);
            queryExecutor.run();
        }
    }

}
