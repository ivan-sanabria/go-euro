/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.go.euro.api.executor;

import com.go.euro.api.external.ApiAdaptor;
import com.go.euro.api.parser.CsvParser;
import com.go.euro.api.utils.CsvValidator;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.AccessDeniedException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

/**
 * Class to handle the test case for executing queries against api.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class QueryExecutorTest {


    @Test
    void execute_query_to_generate_validate_csv() {

        final String keyword = "Berlin"; // Upper letter is used to match using the scanner

        final ApiAdaptor adaptor = k -> {

            final String json = "[\n" +
                    "   {\n" +
                    "      \"_id\":1,\n" +
                    "      \"name\":\"Berlin Tegel\",\n" +
                    "      \"type\":\"airport\",\n" +
                    "      \"geo_position\":{\n" +
                    "         \"latitude\":52.5548,\n" +
                    "         \"longitude\":13.28903\n" +
                    "      }\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"_id\":2,\n" +
                    "      \"name\":\"Berlingo\",\n" +
                    "      \"type\":\"location\",\n" +
                    "      \"geo_position\":{\n" +
                    "         \"latitude\":45.50298,\n" +
                    "         \"longitude\":10.04366\n" +
                    "      }\n" +
                    "   }\n" +
                    "]";

            return JsonParser.parseString(json)
                    .getAsJsonArray();
        };

        final CsvParser parser = new CsvParser();

        final QueryExecutor executor = new QueryExecutor(adaptor, parser, keyword);
        executor.run();

        final String[] args = {keyword};
        CsvValidator.validateTestFiles(args);
    }

    @Test
    void execute_query_to_throw_io_exception() throws IOException {

        final String keyword = "Berlin"; // Upper letter is used to match using the scanner

        final ApiAdaptor adaptor = k -> JsonParser.parseString("[]")
                .getAsJsonArray();

        final CsvParser parser = mock(CsvParser.class);
        final IOException exception = new AccessDeniedException("/incorrect-location&");

        doThrow(exception).when(parser)
                .generateCsv(any(JsonArray.class));

        final QueryExecutor executor = new QueryExecutor(adaptor, parser, keyword);
        executor.run();

        assertTrue(CsvValidator.fileNotFound());
    }

}
