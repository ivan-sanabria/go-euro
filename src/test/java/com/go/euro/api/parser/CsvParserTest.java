/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.go.euro.api.parser;

import com.go.euro.api.utils.CsvValidator;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to handle the test cases for writing content to csv file.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class CsvParserTest {

    /**
     * Define instance for testing purposes.
     */
    private final CsvParser parser = new CsvParser();


    @Test
    void no_file_generation_with_null_result() throws IOException {

        parser.generateCsv(null);
        assertTrue(CsvValidator.fileNotFound());
    }

    @Test
    void no_file_generation_with_empty_result() throws IOException {

        final String json = "[]";

        final JsonArray array = JsonParser.parseString(json)
                .getAsJsonArray();

        parser.generateCsv(array);
        assertTrue(CsvValidator.fileNotFound());
    }

    @Test
    void file_generation_with_array_result() throws IOException {

        final String json = "[\n" +
                "   {\n" +
                "      \"_id\":1,\n" +
                "      \"name\":\"Berlin Tegel\",\n" +
                "      \"type\":\"airport\",\n" +
                "      \"geo_position\":{\n" +
                "         \"latitude\":52.5548,\n" +
                "         \"longitude\":13.28903\n" +
                "      }\n" +
                "   },\n" +
                "   {\n" +
                "      \"_id\":2,\n" +
                "      \"name\":\"Berlingo\",\n" +
                "      \"type\":\"location\"\n" +
                "   }\n" +
                "]";

        final JsonArray array = JsonParser.parseString(json)
                .getAsJsonArray();

        parser.generateCsv(array);

        final String[] args = {"Berlin"};
        CsvValidator.validateTestFiles(args);
    }

}
