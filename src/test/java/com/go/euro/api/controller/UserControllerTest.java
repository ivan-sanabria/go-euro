/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.go.euro.api.controller;

import com.go.euro.api.external.ApiAdaptor;
import com.go.euro.api.utils.CsvValidator;
import com.google.gson.JsonParser;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to handle the test cases, using a specific input from static value and validating the existence of the CSV
 * after the query is made.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class UserControllerTest {


    @Test
    void given_no_query_no_file_generated() {

        final String[] args = {};

        UserController.main(args);
        assertTrue(CsvValidator.fileNotFound());
    }

    @Test
    @SuppressWarnings("AccessStaticViaInstance")
    void given_single_query_single_csv_generated() {

        final String[] args = {"Berlin"}; // Upper letter is used to match using the scanner

        final ApiAdaptor adaptor = k -> {

            final String json = "[\n" +
                    "   {\n" +
                    "      \"_id\":1,\n" +
                    "      \"name\":\"Berlin Tegel\",\n" +
                    "      \"type\":\"airport\",\n" +
                    "      \"geo_position\":{\n" +
                    "         \"latitude\":52.5548,\n" +
                    "         \"longitude\":13.28903\n" +
                    "      }\n" +
                    "   },\n" +
                    "   {\n" +
                    "      \"_id\":2,\n" +
                    "      \"name\":\"Berlingo\",\n" +
                    "      \"type\":\"location\",\n" +
                    "      \"geo_position\":{\n" +
                    "         \"latitude\":45.50298,\n" +
                    "         \"longitude\":10.04366\n" +
                    "      }\n" +
                    "   }\n" +
                    "]";

            return JsonParser.parseString(json)
                    .getAsJsonArray();
        };

        final UserController userController = new UserController();

        userController.setAdaptor(adaptor);
        userController.main(args);

        CsvValidator.validateTestFiles(args);
    }

    @Test
    @SuppressWarnings("AccessStaticViaInstance")
    void given_multiple_queries_multiple_csv_generated() {

        final String[] args = {"Berlin", "Barcelona", "Madrid"}; // Upper letter is used to match using the scanner

        final ApiAdaptor adaptor = k -> {

            String json = "[]";

            if (args[0].equals(k)) {

                json = "[\n" +
                        "   {\n" +
                        "      \"_id\":1,\n" +
                        "      \"name\":\"Berlin Tegel\",\n" +
                        "      \"type\":\"airport\",\n" +
                        "      \"geo_position\":{\n" +
                        "         \"latitude\":52.5548,\n" +
                        "         \"longitude\":13.28903\n" +
                        "      }\n" +
                        "   },\n" +
                        "   {\n" +
                        "      \"_id\":2,\n" +
                        "      \"name\":\"Berlingo\",\n" +
                        "      \"type\":\"location\",\n" +
                        "      \"geo_position\":{\n" +
                        "         \"latitude\":45.50298,\n" +
                        "         \"longitude\":10.04366\n" +
                        "      }\n" +
                        "   }\n" +
                        "]";
            }

            if (args[1].equals(k)) {

                json = "[\n" +
                        "   {\n" +
                        "      \"_id\":378468,\n" +
                        "      \"name\":\"Barcelona\",\n" +
                        "      \"type\":\"location\",\n" +
                        "      \"geo_position\":{\n" +
                        "         \"latitude\":41.38879,\n" +
                        "         \"longitude\":2.15899\n" +
                        "      }\n" +
                        "   },\n" +
                        "   {\n" +
                        "      \"_id\":313711,\n" +
                        "      \"name\":\"Barcelona\",\n" +
                        "      \"type\":\"airport\",\n" +
                        "      \"geo_position\":{\n" +
                        "         \"latitude\":41.3025957,\n" +
                        "         \"longitude\":2.0744297\n" +
                        "      }\n" +
                        "   }\n" +
                        "]";
            }

            if (args[2].equals(k)) {

                json = "[\n" +
                        "   {\n" +
                        "      \"_id\":378655,\n" +
                        "      \"name\":\"Madrid\",\n" +
                        "      \"type\":\"location\",\n" +
                        "      \"geo_position\":{\n" +
                        "         \"latitude\":40.4165,\n" +
                        "         \"longitude\":-3.70256\n" +
                        "      }\n" +
                        "   },\n" +
                        "   {\n" +
                        "      \"_id\":378630,\n" +
                        "      \"name\":\"Las Rosas de Madrid\",\n" +
                        "      \"type\":\"location\",\n" +
                        "      \"geo_position\":{\n" +
                        "         \"latitude\":40.49292,\n" +
                        "         \"longitude\":-3.87371\n" +
                        "      }\n" +
                        "   }\n" +
                        "]";
            }

            return JsonParser.parseString(json)
                    .getAsJsonArray();
        };

        final UserController userController = new UserController();

        userController.setAdaptor(adaptor);
        userController.main(args);

        await().atMost(3, TimeUnit.SECONDS);

        CsvValidator.validateTestFiles(args);
    }

}
